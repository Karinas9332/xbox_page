const prev = document.getElementById('btn-prev'),
    next = document.getElementById('btn-next'),
    slides = document.querySelectorAll('.slide'),
    dots = document.querySelectorAll('.dot');

let index = 0;

function activeSlide(n) {
    for (slide of slides) {
        slide.classList.remove('active');
    }
    slides[n].classList.add('active');
}
function activeDot(ind) {
    for (dot of dots) {
        dot.classList.remove('active');
    }
    dots[ind].classList.add('active');
}
function prepareCurrentSlide(ind) {
    activeSlide(ind);
    activeDot(ind);
}
function nextSlide() {
    if (index == slides.length - 1) {
        index = 0;
        prepareCurrentSlide(index);
    } else {
        index++;
        prepareCurrentSlide(index);
    }
}
function prevSlide() {
    if (index == 0) {
        index = slides.length - 1;
        prepareCurrentSlide(index);
    } else {
        index--;
        prepareCurrentSlide(index);
    }
}
dots.forEach(function (item, indexDot) {
    item.addEventListener('click', function () {
        index = indexDot;
        prepareCurrentSlide(index);
    })
});
next.addEventListener('click', nextSlide);
prev.addEventListener('click', prevSlide);

// const interval = setInterval(nextSlide, 6000);
